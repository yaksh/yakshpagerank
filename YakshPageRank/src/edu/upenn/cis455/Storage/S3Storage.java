package edu.upenn.cis455.Storage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.*;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.Download;
import com.amazonaws.services.s3.transfer.TransferManager;

import edu.upenn.cis455.PageRanker.MainJob;

public class S3Storage {
	
	private AWSCredentials credentials = null;
	public static AmazonS3 s3 = null;
	public static TransferManager transferManager = null;
	private Regions defaultRegion = Regions.US_EAST_1;
	
	public S3Storage(String accessKey, String secretKey) {
		this.credentials = new BasicAWSCredentials(accessKey, secretKey);
		setupS3Client();
		setupTransferManager();
	}
	
	public S3Storage() {
		setupCredentials();
		setupS3Client();
		setupTransferManager();
	}
	
	private void setupCredentials(){
		try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (/home/cis455/.aws/credentials), and is in valid format.",
                    e);
        }
	}
	
	private void setupS3Client(){
		if(credentials != null){
			try{
				s3 = new AmazonS3Client(credentials);
		        Region usEast1 = Region.getRegion(defaultRegion);
		        s3.setRegion(usEast1);
			}
			catch(Exception e){
				System.out.println("Error in getting the S3 client");
				e.printStackTrace();
			}
		}
	}
	
	private void setupTransferManager(){
		transferManager = new TransferManager(credentials);
	}
	
	public void createBucket(String bucketName){
		if(bucketName != null && !bucketName.equals("")){
			if(s3 != null){
				try{
					s3.createBucket(bucketName);
				}
				catch(Exception e){
					System.out.println("Error in creating bucket: " + bucketName);
					e.printStackTrace();
				}
			}	
		}
	}
	
	public java.util.List<Bucket> getBucketList(){
		java.util.List<Bucket> l = null;
		if(s3 != null){
			l = s3.listBuckets();
		}
		return l;
	}
	
	public void upload(String bucket, String key, File file){
		try{
			s3.putObject(new PutObjectRequest(bucket, key, file));
		}
		catch(Exception e){
			System.out.println("Error in storing the object: " + file.getName());
			e.printStackTrace();
		}
	}
	
	/**
	 * @param bucketName
	 * @param key
	 * @return A inputStream from which the data is read
	 */
	public void download(String bucketName, String key, File file){
		try{
			transferManager = new TransferManager(credentials);
			Download myDownload = transferManager.download(bucketName, key, file);
			myDownload.waitForCompletion();
		}
		catch(Exception e){
			System.out.println("Error in fetching the object: " + key);
			e.printStackTrace();
		}
	}
	
	public java.util.List<S3ObjectSummary> getObjectSummary(String bucketName){
		java.util.List<S3ObjectSummary> l = null;
		try{
			l = s3.listObjects(new ListObjectsRequest()
					.withBucketName(bucketName)).getObjectSummaries();
		}
		catch(Exception e){
			System.out.println("Error in fetching the object summaries for: " + bucketName);
			e.printStackTrace();
		}
		return l;
	}
	
	public void deleteObject(String bucket, String key){
		try{
			s3.deleteObject(bucket, key);
		}
		catch(Exception e){
			System.out.println("Error in deleting object: " + key);
			e.printStackTrace();
		}
	}
	
	public void deleteBucket(String bucket){
		try{
			s3.deleteBucket(bucket);
		}
		catch(Exception e){
			System.out.println("Error in deleting bucket " + bucket);
			e.printStackTrace();
		}
	}
	
	public synchronized String getObjectData(S3Object object){
		BufferedReader reader = new BufferedReader(new InputStreamReader(object.getObjectContent()));
		StringBuffer buffer = new StringBuffer();
		try{
			while (true) {
	            String line = reader.readLine();
	            if (line == null) 
	            	break;
	            buffer.append(line);
	        }
		}
		catch(Exception e){
			System.out.println("Error in downloading data from object: " + object.getKey());
			e.printStackTrace();
		}
		finally{
			if(reader != null){
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return (buffer.length() > 0) ? buffer.toString() : "";
	}
	
	public ArrayList<String> getFilesInFolder(String bucket, String folder){
		ArrayList<String> list = new ArrayList<>();
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
	        .withBucketName(bucket)
	        .withPrefix(folder + "/");

        ObjectListing objectListing = s3.listObjects(listObjectsRequest);

		for (S3ObjectSummary summary : objectListing.getObjectSummaries()) {
		    list.add(summary.getKey());
		}
		return list;
	}
	
    public static void main(String[] args) throws IOException {
    	BufferedWriter writer = null;
    	BufferedReader reader = null;
    	try{
        	S3Storage s3 = new S3Storage(args[0], args[1]);
        	ArrayList<String> outKeys = s3.getFilesInFolder(MainJob.outputBucket, "interOutput_jobThree_9");
        	File finalOutput = new File("PageRanks.csv");
        	writer = new BufferedWriter(new FileWriter(finalOutput));
        	for(String key : outKeys){
        		File downloaded = File.createTempFile("rank", ".csv");
        		s3.download(MainJob.outputBucket, key, downloaded);
        		System.out.println("Downloading file " + key);
        		try{
        			reader = new BufferedReader(new FileReader(downloaded));
        			String line = "";
        			System.out.println("Proccessing file " + key);
        			while((line = reader.readLine()) != null){
        				String[] lineBreaks = line.split("\\$\\$\\$\\$", 2);
        				if(lineBreaks.length == 2){
        					writer.write(lineBreaks[0] + "," + lineBreaks[1].split("\t", 2)[0]);
        					writer.newLine();
        				}
        			}
        		}
        		catch(Exception e){
        			System.err.println("Error processing file " + key);
        			e.printStackTrace();
        		}
        		finally{
        			if(reader != null)
        				reader.close();
        			System.out.println("Done Proccessing file " + key);
        		}
        	}
    	}
    	catch(Exception e){
    		System.err.println("Error producing the final output");
    		e.printStackTrace();
    	}
    	finally{
    		if(writer != null)
    			writer.close();
    		if(reader != null)
    			reader.close();
    		System.exit(0);
    	}
    }

    /**
     * Creates a temporary file with text data to demonstrate uploading a file
     * to Amazon S3
     *
     * @return A newly created temporary file with text data.
     *
     * @throws IOException
     */
    private static File createSampleFile() throws IOException {
        File file = File.createTempFile("aws-java-sdk-", ".txt");
        file.deleteOnExit();

        Writer writer = new OutputStreamWriter(new FileOutputStream(file));
        writer.write("abcdefghijklmnopqrstuvwxyz\n");
        writer.write("01234567890112345678901234\n");
        writer.write("!@#$%^&*()-=[]{};':',.<>/?\n");
        writer.write("01234567890112345678901234\n");
        writer.write("abcdefghijklmnopqrstuvwxyz\n");
        writer.close();

        return file;
    }

    /**
     * Displays the contents of the specified input stream as text.
     *
     * @param input
     *            The input stream to display as text.
     *
     * @throws IOException
     */
    private static void displayTextInputStream(InputStream input) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        while (true) {
            String line = reader.readLine();
            if (line == null) break;

            System.out.println("    " + line);
        }
        System.out.println();
    }
    
	public File createTempFile(String prefix, String suffix){
	     String tempDir = System.getProperty("java.io.tmpdir");
	     String fileName = (prefix != null ? prefix : "" ) + (suffix != null ? suffix : "" ) ;
	     return new File(tempDir, fileName);
	}
}
