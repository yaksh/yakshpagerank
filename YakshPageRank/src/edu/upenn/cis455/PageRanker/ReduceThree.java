package edu.upenn.cis455.PageRanker;

import java.io.IOException;
import java.io.*;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class ReduceThree extends Reducer<Text, Text, Text, Text> {
	/*
	 * Input for context to ReduceThree is of the form: 
	 * childUrl childRank 
	 * childUrl childRank
	 * childUrl childRank 
	 * parentUrl1 childUrl1:flag^childUrl2:flag^...
	 * 
	 * Input to ReduceThree
	 * childUrl1 <childRank childRank childRank childRank childRank...>
	 * childUrl2 <childRank childRank childRank childRank childRank...>
	 * parentUrl1 <childUrl1:flag^childUrl2:flag^...>
	 * 
	 */
	
	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException{
		
		double totalScore = (double)0.0;
		String valueStr;
		String childrenStr = "";
		for(Text value : values){
			valueStr = value.toString();
			
			if(valueStr.trim().length() == 0){
				continue;
			}
			
			else if(valueStr.contains("^")){
				childrenStr = valueStr;
				continue;
			}
			
			else{
				totalScore += Double.parseDouble(valueStr);
			}
		}
		totalScore = (double)(0.15 + 0.85 * (totalScore));
		context.write(new Text(key + "$$$$" + totalScore), new Text(childrenStr));
	}
}
