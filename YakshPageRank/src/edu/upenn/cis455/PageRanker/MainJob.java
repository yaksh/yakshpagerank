package edu.upenn.cis455.PageRanker;

import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Date;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import com.amazonaws.services.s3.model.S3ObjectSummary;

import edu.upenn.cis455.Storage.S3Storage;


public class MainJob {
	
	public static String awsAccessKey;
	public static String awsSecretKey;
	public static String inputPath;
	public static String outputPath;
	public static String outputBucket = "yaksh";
	
	/*
	 * Function to concatenate all files for PageRank input
	 */
	public ArrayList<File> combinePageRankFiles(String inputDir) throws IOException{
		ArrayList<File> files = new ArrayList<>();
		//for all pageRank files put it into one file
		final String dir = System.getProperty("user.dir");
        System.out.println("current dir = " + dir);
		for(File f : new File(inputDir).listFiles()){
			if(f.getAbsolutePath().endsWith(".crc"))
				continue;
			if(f.getAbsolutePath().contains("_SUCCESS"))
				continue;
			System.out.println("[input files]" + f.getAbsolutePath());
			files.add(f);
		}
		return files;
	}	
	
	/*
	 * Function to run a map reduce job
	 */
	public Job runJob(Class Mapper, Class Reducer, ArrayList<String> inputDir, String output) throws IllegalArgumentException, IOException {
		Configuration conf = new Configuration();
		//@SuppressWarnings("deprecation")
		Job job = new Job(conf, "pageRank");
		job.setJarByClass(MainJob.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setInputFormatClass(KeyValueTextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setMapperClass(Mapper);
		job.setReducerClass(Reducer);
		
		for(String f : inputDir){
			FileInputFormat.addInputPath(job, new Path(f));
		}
		FileOutputFormat.setOutputPath(job, new Path(output));
		return job;
	}
	
	/*
	 * Main function that takes in 3 arguments
	 * first - number of iterations to run the job for
	 * second - the path to the input file
	 * third - the path to store the outputs 
	 */
	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException{
		
		MainJob.awsAccessKey = args[0];
		MainJob.awsSecretKey = args[1];
		int numIterations = Integer.parseInt(args[2]);
		MainJob.inputPath = args[3];	//"yaksh-pagerank";
		MainJob.outputPath = args[4]; //"s3n://yaksh/PageRankOutput"
		
		String intermediatePath = "";
		
		S3Storage s3 = new S3Storage(MainJob.awsAccessKey, MainJob.awsSecretKey);
		
		java.util.List<S3ObjectSummary> inputs = s3.getObjectSummary(inputPath);
		
		MainJob pageRank = new MainJob();
		
		//Fetch all the keys for a given bucket
		ArrayList<String> inputKeys = pageRank.getFilesBucket(inputs);
		
		intermediatePath = outputPath.substring(0, outputPath.lastIndexOf('/')) + "/interOutput";
		String outputFilesJob1 = intermediatePath + "_jobOne";

		//intermediatePath = outputPath + "/interOutput";
		System.out.println("INTERMEDIATE_PATH" + intermediatePath);
		
		//First Map/Reduce
		long a = new Date().getTime();
		Job job = pageRank.runJob(MapOne.class, ReduceOne.class, inputKeys, outputFilesJob1);
		
		System.out.println("WAITING FOR JOB 1");
		job.waitForCompletion(true);
		
		long b = new Date().getTime();
		System.out.println("First job done in " + (b-a)/6000 +  " mins");
		
		ArrayList<String> inputKeys2 = s3.getFilesInFolder(MainJob.outputBucket, "interOutput_jobOne");
		
		ArrayList<String> inputKeysJob2 = pageRank.createKey(inputKeys2, MainJob.outputBucket);
		
		String outputFilesJob2 = intermediatePath + "_jobTwo";
		
		//Second Map/Reduce
		a = new Date().getTime();
		job = pageRank.runJob(MapTwo.class, ReduceTwo.class, inputKeysJob2, outputFilesJob2);
		
		System.out.println("WAITING FOR JOB 2");
		job.waitForCompletion(true);
		
		b = new Date().getTime();
		System.out.println("Second job done in " + (b-a)/6000 + " mins");
		
		ArrayList<String> inputKeys3 = s3.getFilesInFolder(MainJob.outputBucket, "interOutput_jobTwo");
		
		ArrayList<String> inputKeysJob3 = pageRank.createKey(inputKeys3, MainJob.outputBucket);
		
		String outputFilesJob3 = intermediatePath + "_jobThree_0";
		
		String prevFolder = "";
		
		//Third Map/reduce... the iteration phase
		a = new Date().getTime();
		for(int i = 0; i < numIterations; i++) {
			System.out.println("[DEBUG] Iteration NUMBER:" + (i + 1));			
			prevFolder = "interOutput_jobThree_" + i;
			job = pageRank.runJob(MapThree.class, ReduceThree.class, inputKeysJob3, outputFilesJob3);
			System.out.println("WAITING FOR JOB 3");
			job.waitForCompletion(true);
			ArrayList<String> inputKeysJobFinal = s3.getFilesInFolder(MainJob.outputBucket, prevFolder);
			inputKeysJob3 = pageRank.createKey(inputKeysJobFinal, MainJob.outputBucket);
			outputFilesJob3 = intermediatePath + "_jobThree" +  "_" + (i + 1);
		}
	}
	
	public ArrayList<String> getFilesBucket(java.util.List<S3ObjectSummary> inputs){
		ArrayList<String> files = new ArrayList<>();
		if(inputs.size() > 0){
			for(S3ObjectSummary object : inputs){
				files.add("s3n://" + MainJob.inputPath + "/" + object.getKey());
			}
		}
		return files;
	}
	
	public ArrayList<String> createKey(ArrayList<String> inKey, String bucket){
		ArrayList<String> list = new ArrayList<>();
		for(String key : inKey){
			if(!key.endsWith("_SUCCESS"))
				list.add("s3n://" + bucket + "/" + key);
		}
		return list;
	}
}
