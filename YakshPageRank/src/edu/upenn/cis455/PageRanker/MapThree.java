package edu.upenn.cis455.PageRanker;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MapThree extends Mapper<Text, Text, Text, Text> {

	/**
	 * Input to MapThree is of the form: parentUrl1:initialScore
	 * childUrl1:flag^childUrl2:flag^...
	 * 
	 * Output of MapThree is of the form: childUrl childRank childUrl childRank
	 * childUrl childRank parentUrl1 childUrl1:flag^childUrl2:flag^...
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * 
	 */

	public void map(Text key, Text value, Context context) throws IOException,
			InterruptedException {
		System.out.println("IN MAPPER THREE : key : " + key + "  value : " + value);
		/*
		 * String[] splitKey = key.toString().split("$$$$"); String parent =
		 * splitKey[0]; double parentRank = Double.parseDouble(splitKey[1]);
		 * String childrenStr = value.toString();
		 */
		String[] splitKey = null;
		String parent = null;
		double parentRank = 0;
		String childrenStr = value.toString();

		if (key.toString() != null) {
			splitKey = key.toString().split("\\$\\$\\$\\$");
			System.out.println(splitKey[0]);
		}
		
		//System.out.println("ENDING PREVIOUS 4 length: "  + splitKey.length);

		if (splitKey.length == 2) {
			//System.out.println("ENDING PREVIOUS 5");
			if (splitKey[0] != null && !splitKey[0].equals("null")) {
				parent = splitKey[0];
			}

			if (splitKey[1] != null && !splitKey[0].equals("null")) {
				parentRank = Double.parseDouble(splitKey[1]);
			}

			// checking if children exist to the parent url
			if (!childrenStr.contains("^")) {
				//System.out.println("ENDING PREVIOUS 2");
				context.write(new Text(parent), new Text(splitKey[1]));
			}
			
			//System.out.println("ENDING PREVIOUS 3");

			if(!childrenStr.equals("") && childrenStr != null){
				double childRank;
				if(childrenStr.contains("^")){
					String[] children = childrenStr.split("\\^");
					int numChildren = 0;
					for (String child : children) {
						if (child != " ") {
							try {
								if (child.contains("$$$$")) {
									String[] tokens = child
											.split("\\$\\$\\$\\$");
									if (tokens.length == 2) {

										System.out.println("CHILD is : "
												+ child);

										if (child.split("\\$\\$\\$\\$")[1]
												.equals("0")) {
											continue;
										}
										numChildren++;

									}
								}
							} catch (ArrayIndexOutOfBoundsException e) {
								System.out.println("Child format ERROR : "
										+ key.toString() + " : "
										+ value.toString());
							}
						}
					}
					System.out.println("CHILDREN # : " + numChildren);
					childRank = (numChildren == 0) ? (double) 0.15	: (double) parentRank / numChildren;
					System.out.println("CHILD RANK is : " + childRank);
					
					for (String child : children) {
						if(child.contains("$$$$")){
							String[] tokens = child.split("\\$\\$\\$\\$");
							if(tokens.length == 2){
								if (child.split("\\$\\$\\$\\$")[1].equals("0")) {
									continue;
								} else {
									//childUrl childRank
									System.out.println("\n");
									context.write(new Text(child.split("\\$\\$\\$\\$")[0]), new Text(
											Double.toString(childRank)));
								}
							}
						}
					}
				}
			}
			// parentUrl1 childUrl1:flag^childUrl2:flag^...
			// so that when iterations take place parent has a record of its
			// child
			System.out.println("ENDING HERE");
			System.out.println("\n");
			System.out.println("\n");
			context.write(new Text(parent), value);
		}

	}

}
