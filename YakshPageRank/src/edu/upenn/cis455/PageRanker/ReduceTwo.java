package edu.upenn.cis455.PageRanker;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class ReduceTwo extends Reducer <Text , Text, Text, Text>{
	
	/**
	 * Input to ReduceTwo after global sorting is of the form:
	 * parentUrl1	childUrl1$$$$flag
	 * parentUrl1	childUrl2$$$$flag
	 * ..
	 * 
	 * Output of ReduceTwo is of the form:
	 * parentUrl1$$$$initialScore	childUrl1$$$$flag^childUrl2$$$$flag^...
	 * 
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException{
		String value = "";
		for(Text child : values){
			value += child.toString() + "^";
		}
		context.write(new Text(key + "$$$$1.0"), new Text(value));
	}
}
