/**
 * 
 */
package edu.upenn.cis455.PageRanker;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author cis455
 *
 */
public class PageRankerTemplate {

	ArrayList<Page> frontier = new ArrayList<Page>();
	Page ether;
	
	public PageRankerTemplate(){
		frontier = new ArrayList<Page>();
		ether = getEther();
	}
	
	
	public void breadthFirstSearch(Page currentPage){
		
		visit(currentPage);
		frontier.addAll(currentPage.fanout);
		frontier.remove(0);
		
		if(!frontier.isEmpty()) {
			breadthFirstSearch(frontier.get(0));
		}
	}
	
	private void visit(Page currentPage){
		
		//sanatizePage(currentPage);

		// update all fanOut links with the weights
		for(Page page : currentPage.fanout){
			page.fanIn.put(currentPage, currentPage.weight/currentPage.fanout.size());
		}

		// update page weight by reading all incoming weights
		double sum = 0;
		if(currentPage.fanIn != null && currentPage.fanIn.keySet().size() > 0){
			for(Page page : currentPage.fanIn.keySet()){
				sum +=  currentPage.fanIn.get(page);
			}
		}
		currentPage.weight = sum;

		// print currentPage as visited
		System.out.println("("+currentPage.url+","+currentPage.weight+")");
	}
	
	public Page getEther(){
		ArrayList<Page> seedPages = new ArrayList<Page>();
		
		
		// yahoo
		Page yahoo = new Page();
		yahoo.url = "yahoo";
		yahoo.weight = 0;
		yahoo.parent = null;
		seedPages.add(yahoo);
				
		// google
		Page google = new Page();
		google.url = "google";
		google.weight = 0;
		google.parent = null;
		seedPages.add(google);
		
		// amazon
		Page amazon = new Page();
		amazon.url = "amazon";
		amazon.weight = 0;
		amazon.parent = null;
		seedPages.add(amazon);
		
		
		// topology
		google.fanout.add(amazon);
		amazon.fanout.add(google);
		amazon.fanout.add(yahoo);
		yahoo.fanout.add(google);
		
		Page ether = new Page();
		ether.url = "ether";
		ether.weight = 1.0;
		ether.parent = new Page();
				
		if(seedPages != null)
			ether.fanout.addAll(seedPages);
		
		return ether;
	}
	
	public void seed(){
		frontier.add(ether);
		breadthFirstSearch(ether);
	}
	
	public void sanatizePage(Page currentPage){
		if(currentPage.fanout == null)
			currentPage.fanout = new ArrayList<Page>();
		
		if(currentPage.fanout.contains(currentPage))
			currentPage.fanout.removeAll(Collections.singleton(currentPage));
		
		if(currentPage.fanout.size() == 0)
			currentPage.fanout.add(ether);
		
	}
	


	public static void main(String[] args){
		
		PageRankerTemplate pageRanker = new PageRankerTemplate();
		pageRanker.seed();
	}
}
