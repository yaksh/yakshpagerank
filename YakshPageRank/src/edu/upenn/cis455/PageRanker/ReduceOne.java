package edu.upenn.cis455.PageRanker;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class ReduceOne extends Reducer<Text, Text, Text, Text>{
	
	/**
	 * Input to ReducerOne after global sorting is of the form:
	 * childUrl	parentUrl1
	 * childUrl	parentUrl2
	 * childUrl	PARENT
	 * ..
	 * 
	 * Output of ReducerOne is of the form:
	 * parentUrl1	childUrl$$$$flag
	 * parentUrl2	childUrl$$$$flag
	 * 
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	
	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException{
		//flag = 0, indicates the child is a dangling url and 
		//flag = 1 indicates a non dangling child
		String flag = "0";
		String child = key.toString();
		ArrayList<String> parents = new ArrayList<String>();
		
		for(Text parent : values){
			if(parent.toString().equals("PARENT")){
				flag = "1";
				continue;
			}
			//adding all parentUrl to an array list
			parents.add(parent.toString());	
		}
	
		for(String outputKey : parents){
			if((outputKey.toString().trim().length() > 0) && (outputKey.toString().startsWith("http")) && 
					(child.toString().trim().length() > 0) && (child.toString().startsWith("http"))){
				context.write(new Text(outputKey), new Text(child + "$$$$" + flag));
			}
		}
	}
}
