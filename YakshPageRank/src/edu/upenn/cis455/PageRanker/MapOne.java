package edu.upenn.cis455.PageRanker;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MapOne extends Mapper<Text, Text, Text, Text>{
	
	/**
	 * Input to MapOne is of the form:
	 * parentUrl	childUrl1;;childUrl2;;childUrl3;;.....
	 * 
	 * Output of MapOne is of the form:
	 * childUrl1	parentUrl
	 * childUrl2	parentUrl
	 * childUrl3	parentUrl
	 * ..
	 * parentUrl	PARENT
	 * 
	 * 
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	
	public void map(Text key, Text value, Context context) throws IOException, InterruptedException{
		Text parentUrl = new Text(key.toString());
		String[] children = value.toString().split(";;");
		for(String child : children){
			if(child.equals(null) || child.equals("null")){
				continue;
			}
			Text childUrl = new Text(child);
			context.write(childUrl, parentUrl);
		}
		Text parentIndicator = new Text("PARENT");
		if((parentUrl.toString().trim().length() > 0) && parentUrl.toString().startsWith("http")){
			context.write(parentUrl, parentIndicator);
		}
	}
	
}
