package edu.upenn.cis455.PageRanker;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MapTwo extends Mapper<Text, Text, Text, Text>{
	
	/*
	 * Basically Passes output of ReduceOne to ReduceTwo directly 
	 */
	
	public void map(Text key, Text value, Context context) throws IOException, InterruptedException{
		context.write(key, value);
	}
}
